# hdmiControl

Automatic turn on the TV and open a http web site on browser after desktop environment start.

## Requirements

Follow the instructions to install CEC library [here](https://github.com/Pulse-Eight/libcec) and git clone [this](https://github.com/maurucao/hdmiControl).

## Usage

Raspbian - execute the startup.sh at the end of lxsession script.

## commands

scan for devices connected - `echo scan | cec-client -s -d 1`

## Links

* http://raspberrypi.stackexchange.com/questions/9142/commands-for-using-cec-client

* http://www.howtogeek.com/207186/how-to-enable-hdmi-cec-on-your-tv-and-why-you-should/

* http://raspberrypi.stackexchange.com/questions/10371/no-hdmi-output-when-hdmi-cable-is-connected-after-power-supply

* http://raspberrypi.stackexchange.com/questions/2169/how-do-i-force-the-raspberry-pi-to-turn-on-hdmi
